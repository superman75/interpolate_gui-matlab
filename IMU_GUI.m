function varargout = IMU_GUI(varargin)
clc;
format;
% IMU_GUI MATLAB code for IMU_GUI.fig
%      IMU_GUI, by itself, creates a new IMU_GUI or raises the existing
%      singleton*.
%
%      H = IMU_GUI returns the handle to a new IMU_GUI or the handle to
%      the existing singleton*.
%
%      IMU_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in IMU_GUI.M with the given input arguments.
%
%      IMU_GUI('Property','Value',...) creates a new IMU_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before IMU_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to IMU_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help IMU_GUI

% Last Modified by GUIDE v2.5 17-Dec-2018 18:15:53

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @IMU_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @IMU_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before IMU_GUI is made visible.
function IMU_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to IMU_GUI (see VARARGIN)

% Choose default command line output for IMU_GUI
handles.output = hObject;
set(handles.pushbutton2,'Enable','off');
set(handles.edit2,'String','1092');
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes IMU_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = IMU_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

clc;
set(handles.text2,'String','');
% get the folder
folder_name = uigetdir;
disp(folder_name);
handles.folder_name = folder_name;

if folder_name ~= 0
   
    set(handles.edit1,'String',folder_name);
    data = dir(folder_name);
    MyListOfFiles = [];
    for k=1:length(data)
        if data(k).isdir==0
            MyListOfFiles{end+1,1} = data(k).name;
        end
    %     baseFileName = data(k).rawdata;
    %     fullFileName = fullfile(files, baseFileName);
    %     textarray=textscan(fullFileName);
    end
    % update the listbox with the result
    if isempty(MyListOfFiles)
        set(handles.listbox1,'String','It is an empty folder.');
    else
        set(handles.listbox1,'String',MyListOfFiles)
    end
    set(handles.pushbutton2,'Enable','on');
else
    
    set(handles.text2, 'ForegroundColor','red' );
    set(handles.text2,'String','Please choose folder');
    set(handles.pushbutton2,'Enable','off');
end


guidata(hObject, handles);
function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double

guidata(hObject, handles);
% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, ~, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
guidata(hObject, handles);

function listbox1_Callback(hObject, ~,handles)





% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, ~, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% get folder name
folder_name = handles.folder_name;
data = dir(folder_name);

% get sampling frequency from user
Fs1 = str2num(get(handles.edit2,'String'));

% if sampling frequency is empty, 
if isempty(Fs1)
    set(handles.text2, 'ForegroundColor','red' );
    set(handles.text2,'String','please input valid number');
else
    % text handling flag
    flag = 1;   
    set(handles.figure1, 'pointer', 'watch');

    set(handles.text2, 'ForegroundColor','blue' );
    set(handles.text2,'String','please wait....');
    drawnow;
    % calc for each csv file
    for i = 1 : length(data)
        
        % get csv file path
        filename = strcat(strcat(folder_name,'\'), data(i).name);
        disp(filename);
        % if there is no csv file on the selected folder, break for-loop
        if ~isfile(filename)
            if i==1 
                flag = 1;
            end
            continue;
        end
        % get all data from selected csv file
        M = csvread(filename,1,0);
        count(i) = size(M,1);
        if flag == 0
            M = [M1;M];
        end
        
        set(handles.text2,'String',filename);
        drawnow;
        M1 = csvread(filename,count(i),0);
        flag = 0;
        % extract each data like time, XGyro, YGyro etc ...
        time = M(:,2);
        L = size(time, 1);
        dtime = diff(time);
%         Fs = 1/dtime(1);
        I = find(dtime>2/Fs1);
%         fprintf(fid,'%d\n',I);
        xgyro = M(:,3);
        ygyro = M(:,4);
        zgyro = M(:,5);
        xacc = M(:,6);
        yacc = M(:,7);
        zacc = M(:,8);
        
        %initialize the array variables
        gap = [];     
        
        % evaluate the missing data
        for m = 1 : length(I)
            gap = [gap (time(I(m)) + 1/Fs1):1/Fs1:time(I(m)+1)];
              
        end
        
        xgyro_gap = interp1(time, xgyro, gap, 'spline','extrap');
        ygyro_gap = interp1(time, ygyro, gap, 'spline','extrap'); 
        zgyro_gap = interp1(time, zgyro, gap, 'spline','extrap'); 
        xacc_gap = interp1(time, xacc, gap, 'spline','extrap'); 
        yacc_gap = interp1(time, yacc, gap, 'spline','extrap'); 
        zacc_gap = interp1(time, zacc, gap, 'spline','extrap'); 

        % output to result.csv file
        M = [zeros(size(gap')) gap' xgyro_gap' ygyro_gap' zgyro_gap' xacc_gap' yacc_gap' zacc_gap'];
        dlmwrite(filename,M,'-append','precision','%.8f');
        
        M = csvread(filename,1,0);
        M = sortrows(M,2);
        
        dlmwrite(filename,M,'precision','%.8f');

%         % get fft
%         n = 2^nextpow2(L);
%         X = [xgyro'; ygyro'; zgyro'];
%         dim = 2;
% 
%         Y = fft(X,n,dim);
%         X1 = ifft(Y);
% 
%         P2 = abs(Y/L);
%         P1 = P2(:,1:n/2+1);
%         P1(:,2:end-1) = 2*P1(:,2:end-1);
%         figure;
%         for j=1:3
%             subplot(3,1,j)
%             plot(Fs1/n:(Fs1/n):(Fs1/2-Fs1/n),P1(j,2:n/2))
%             title(['Row ',num2str(j),' in the Frequency Domain'])
%         end

%         plot gyros 
%         figure;
%         plot(time, xgyro, 'r*', gap, xgyro_gap, 'b*',time, ygyro , 'g*', gap, ygyro_gap , 'y*',time, zgyro , 'm*', gap, zgyro_gap, 'c*');
%         legend('xgyro', 'xgyro-gap', 'ygyro', 'ygyro-gap', 'zgyro', 'zgyro-gap');
%         hold on;


%       plot acc 
  
%         plot(time, xacc, 'r*', gap, xacc_gap, 'b*',time, yacc , 'g*', gap, yacc_gap , 'y*',time, zacc , 'm*', gap, zacc_gap, 'c*');
%         legend('xgyro', 'xgyro-gap', 'ygyro', 'ygyro-gap', 'zgyro', 'zgyro-gap');
%         hold on;
    end
    set(handles.figure1, 'pointer', 'arrow');
    if flag == 0 
        set( handles.text2, 'ForegroundColor','green' );
        set(handles.text2,'String','Success');
    else 
        set( handles.text2, 'ForegroundColor','red' );
        set(handles.text2,'String','There is no IMU data');
    end
    
end

% guidata(hObject, handles);



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on key press with focus on pushbutton2 and none of its controls.
function pushbutton2_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
%     disp('please wait');
